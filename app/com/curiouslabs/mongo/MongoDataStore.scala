package com.curiouslabs.mongo

import reactivemongo.api._
import reactivemongo.api.collections.bson.BSONCollection

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

/**
  * Created by visitor15 on 11/8/16.
  */
case class MongoDataStore(server:               String,
                          db:                   String,
                          port:                 String,
                          connectTimeout:       Int,
                          authSource:           Option[String],
                          sslEnabled:           Boolean,
                          sslAllowInvalidCert:  Boolean,
                          tcpNoDelay:           Boolean,
                          keepAlive:            Boolean,
                          nbChannelsPerNode:    Int,
                          readPreference:       ReadPreference,
                          defaultDB:            Future[DefaultDB],
                          collectionName:       String) {
  val collection: BSONCollection = Await.result(defaultDB.map(defDB => defDB(collectionName)), 10.seconds)
}
