package com.curiouslabs.mongo

import reactivemongo.api._

import scala.concurrent.Future

/**
  * Created by nchampagne on 1/30/17.
  */
trait DataStoreConfiguration {
  def server:               String
  def db:                   String
  def port:                 String
  def connectTimeout:       Int
  def authSource:           Option[String]
  def sslEnabled:           Boolean
  def sslAllowInvalidCert:  Boolean
  def tcpNoDelay:           Boolean
  def keepAlive:            Boolean
  def nbChannelsPerNode:    Int
  def readPreference:       ReadPreference
  def mongoOptions:         MongoConnectionOptions

  def mongoDriver:          MongoDriver
  def mongoConnection:      MongoConnection
  def defaultDB:            Option[Future[DefaultDB]]

  def getMongoDataStore(configName: String): MongoDataStore
}
