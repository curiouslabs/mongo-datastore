package com.curiouslabs.mongo

import javax.inject.{Inject, Singleton}

import play.api.Configuration
import reactivemongo.api.{CrAuthentication, MongoConnectionOptions, MongoDriver, ReadPreference}

import scala.concurrent.ExecutionContext.Implicits.global

/**
  * Created by visitor15 on 11/8/16.
  */
@Singleton
class DefaultMongoConfiguration @Inject()(config: Configuration) extends DataStoreConfiguration {
  val server              = config.getString("db.mongodb.host").getOrElse("")
  val db                  = config.getString("db.mongodb.db").getOrElse("")
  val port                = config.getString("db.mongodb.port").getOrElse("")
  val connectTimeout      = config.getInt("db.mongodb.connectTimeout").getOrElse(1000)
  val authSource          = config.getString("db.mongodb.authSource")
  val sslEnabled          = config.getBoolean("db.mongodb.sslEnabled").getOrElse(false)
  val sslAllowInvalidCert = config.getBoolean("db.mongodb.sslAllowInvalidCert").getOrElse(false)
  val tcpNoDelay          = config.getBoolean("db.mongodb.tcpNoDelay").getOrElse(false)
  val keepAlive           = config.getBoolean("db.mongodb.keepAlive").getOrElse(false)
  val nbChannelsPerNode   = config.getInt("db.mongodb.nbChannelsPerNode").getOrElse(10)
  val readPreference      = config.getString("db.mongodb.readPreference") match {
    case Some("primaryPreference")  => ReadPreference.primaryPreferred
    case Some("primary")            => ReadPreference.primary
    case Some("secondary")          => ReadPreference.secondary
    case Some("nearest")            => ReadPreference.nearest
    case _                          => ReadPreference.primary
  }

  val mongoOptions = MongoConnectionOptions(
    connectTimeout,
    authSource,
    sslEnabled,
    sslAllowInvalidCert,
    CrAuthentication,
    tcpNoDelay,
    keepAlive,
    nbChannelsPerNode
  )

  val mongoDriver     = new MongoDriver()
  val mongoConnection = mongoDriver.connection(List(s"$server:$port"))
  val defaultDB       = Some(mongoConnection.database(db))

  def getMongoDataStore(configName: String) = {
    val collectionName = config.getString(s"db.mongodb.collections.$configName")
    val mongoDataStore = for {
      collName      <- collectionName
      dbConnection  <- defaultDB
    } yield {
      MongoDataStore(server,
        db,
        port,
        connectTimeout,
        authSource,
        sslEnabled,
        sslAllowInvalidCert,
        tcpNoDelay,
        keepAlive,
        nbChannelsPerNode,
        readPreference,
        dbConnection,
        collName)
    }

    mongoDataStore match {
      case Some(dataStore)  => dataStore
      case None             => throw new Exception("Error initializing MongoDataStore")
    }
  }
}
