package com.curiouslabs.mongo

import reactivemongo.api.QueryOpts
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.{BSONDocument, BSONDocumentReader, BSONDocumentWriter}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
  * Created by visitor15 on 11/12/16.
  */
trait CrudRepository[T] {

  protected def save(entity: T)(query: => BSONDocument)(implicit collection: BSONCollection, bSONDocumentWriter: BSONDocumentWriter[T]): Future[WriteResult] = {
    collection.update(query, BSONDocument("$set" -> entity), upsert = true, multi = false)
  }

  protected def readOne(query: => BSONDocument)(implicit collection: BSONCollection, bSONDocumentReader: BSONDocumentReader[T]): Future[Option[T]] = {
    collection.find(query).one[T]
  }

  protected def readMany(limit: Option[Int] = None, skip: Option[Int] = None)(query: => BSONDocument)(implicit collection: BSONCollection, bSONDocumentReader: BSONDocumentReader[T]): Future[List[T]] = {
    collection.find(query).options(QueryOpts(skipN = skip.getOrElse(0))).cursor[T]().collect[List](limit.getOrElse(-1), true)
  }
}
